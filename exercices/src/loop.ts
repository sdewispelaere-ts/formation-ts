// Implémenter cette classe
// Faire l'exercice en TDD (voir loop.spec.ts)
export class Loop<T> {
    // Ajoute un élément A au début de la boucle : B => C => D => B devient A => B => C => D => A
    push(t: T) {
    }

    // Retourne la valeur et la supprime de la boucle : A => B => C => A devient B => C => B et renvoie A
    pop(): T {
        return <T>{};
    }

    // Retourne la valeur sans la supprimer 
    peek(): T{
        return <T>{};
    }

    // Fait tourner la boucle vers la gauche : A => B => C => A devient B => C => A => B
    turnLeft() {

    }

    // Fait tourner la boucle vers la droite : A => B => C => A devient C => A => B => C
    turnRight() {
    }
}