async function wait(milliseconds: number) {
    // Cette fonction crée la Promise à la main
    return new Promise((resolve, reject) => {
        // On doit appeler resolve quand l'opération est finie (reject si ça échoue)
        // Ici on passe le callback à la fonction setTimeout()
        setTimeout(resolve, milliseconds);
    });
}

// 1) En utilisant la fonction wait() ci-dessus, écrire une fonction qui décompte depuis 5 jusqu'à 1 puis affiche "Partez !", avec un intervale de 1s entre deux affichages

async function main() {
}

main();

// 2) Dans un deuxième temps, refactorisez main() pour prendre une fonction en paramètre qui renvoie les messages à afficher
// (par exemple la fonction message() ci-dessous)

function* message() {
    yield "Prêts ?";
    yield "Partez"!;
}
// 3) La fonction passée en paramètre doit maintenant renvoyer un tableau [string, number], avec le texte à afficher et le temps à attendre

function* messageAndTime(): IterableIterator<[string, number]> {
    yield ["Prêts ?", 1000];
    yield ["Partez !", 1000];
}

// 4) Essayez également en renvoyant un objet avec une propriété message et une propriété time
// 5) Considérons les classes suivantes :
class Tower {
    constructor(protected height: number) {}
}

interface Launcher {
    launch():void;
}

interface Vehicle {
    name: string;
}

interface Rocket extends Vehicle {
    stages: number;
}

class ControlTower<T extends Vehicle> extends Tower implements Launcher {
    vehicleOnLaunchPad: T | undefined;

    constructor() {
        super(15);
    }

    putOnPad(vehicle: T) {
        this.vehicleOnLaunchPad = vehicle;
    }

    launch() {
        if (this.vehicleOnLaunchPad) {
            console.log(/* message qui contient le nom du véhicule (utiliser `${...}`) */);
            this.vehicleOnLaunchPad = undefined;
        }
    }
}
// Ecrivez un exemple d'utilisation de cette classe pour une fusée et appelez les méthodes putOnPad and launch

// 6) Ecrivez une fonction qui à partir d'un constructeur qui renvoie Launcher, renvoie un constructeur qui renvoie
// une classe qui rajoute une méthode async countDown() qui appelle launch au bout du décompte déjà utilisé
function CountDown<T extends new(...args: any[]) => Launcher>(Base: T) {
    return class extends Base {        
    };
}
// Et utilisez cette nouvelle classe dans votre exemple précédent. Appelez la méthode countDown() à la place de launch()

