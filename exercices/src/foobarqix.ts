export class FooBarQix {
    public Get(value: number):string {
        return value.toString();
    }
}

// Exercice : 
// Convertir la classe C# ci-dessous et vérifier que les TU passent

// namespace Kata2
// {
//     using System.Collections.Generic;
//     using System.Linq;

//     public class FooBarQix
//     {
//         private IEnumerable<int> DigitsFromLeastToMostSignificant(int value)
//         {
//             while (value > 0)
//             {
//                 yield return value % 10;
//                 value /= 10;
//             }
//         }

//         /// <summary>Converts a digit to a text, if any</summary>
//         /// <param name="digit">The digit</param>
//         /// <returns>The text or empty</returns>
//         private IEnumerable<string> DigitToText(int digit)
//         {
//             if (digit == 3) yield return "Foo";
//             if (digit == 5) yield return "Bar";
//             if (digit == 7) yield return "Qix";
//         }

//         /// <summary>Gets the different dividers in reverse order</summary>
//         /// <param name="value">The value</param>
//         /// <returns>The list of dividers if any</returns>
//         private IEnumerable<string> GetDividers(int value)
//         {
//             if (value % 7 == 0) yield return "Qix";
//             if (value % 5 == 0) yield return "Bar";
//             if (value % 3 == 0) yield return "Foo";
//         }

//         public string Get(int value)
//         {
//             IEnumerable<string> digits = DigitsFromLeastToMostSignificant(value).SelectMany(DigitToText);
//             IEnumerable<string> dividers = this.GetDividers(value);

//             // All the rules are treated in reverse because it's easier to read the digits from right to left
//             var values = digits.Concat(dividers).Reverse().ToArray();

//             if (values.Any())
//             {
//                 return string.Join("", values);
//             }

//             return value.ToString();
//         }
//     }
// }
