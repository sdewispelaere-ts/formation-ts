import { test } from "ava";

test.failing("simple fonction sans paramètre", t => {
    // const fonction = ???;
    // Tenter de spécifier explicitement le type de la variable
    // t.is(fonction(), "test");
})

test.failing("un paramètre de fonction", t => {
    // const repeat = ???;
    // t.is(repeat("to"), "toto");
})

test.failing("plusieurs paramètres de fonction", t => {
    // const somme = ???;
    // t.is(somme(3, 4), 7);
})

class Example {
    a = 3;
    b = function(this: Example) {
        // On est obligé de spécifier le this attendu
        return this.a;
    }
}

class Test {
    a = 5;
    constructor(public b: () => number) {
    }
}

test.failing("le this n'est pas capturé dans une classe avec une fonction normale", t => {
    const example = new Example();
    const test = new Test(example.b);
    t.is(test.b(), 3);
});
