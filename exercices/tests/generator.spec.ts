import { test } from "ava";

// 51: Generator - Yield Expressions
// To do: make all tests pass, leave the assert lines unchanged!

function* generatorFunction() {
    yield 'hello';
    yield 'world';
}

let generator: IterableIterator<string>;

test.beforeEach('generator - `yield` is used to pause and resume a generator function', () => {
    generator = generatorFunction();
}); 
    
test.failing('converting a generator to an array resumes the generator until all values are received', t => {
    let values = Array.from(<any>{});
    t.deepEqual(values, ['hello', 'world']);
});
    
test.failing('after the first `generator.next()` call, the value is "hello" and done is false', t => {
    const {value, done} = {value: "toto", done: true};
    t.is(value, 'hello');
    t.is(done, false);
});
    
test.failing('after the second `next()` call', t => {
    const secondItem = generator.next();

    let {value, done } = {value: "toto", done: true};
    t.is(value, 'world');
    t.is(done, false);
});
    
test.failing('after stepping past the last element, calling `next()` that often, `done` property equals true, since there is nothing more to iterator over', t => {
    generator.next();
    generator.next();
    let done = false;
    t.is(done, true);
});
