import { test, TestContext } from "ava";
import { FooBarQix} from "../src/foobarqix";

function testEquals(t: TestContext, input: number, expected: string) {
    const foobarqix = new FooBarQix();
    t.is(foobarqix.Get(input), expected);
}

function testContains(t: TestContext, input: number, expected: string) {
    const foobarqix = new FooBarQix();
    t.true(foobarqix.Get(input).indexOf(expected) >= 0);
}

test.failing("FooBarQix_NumberObeyNoRule_DisplayIt", testEquals, 1, "1");
test.failing("FooBarQix_NumberObeyNoRule_DisplayIt", testEquals, 2, "2");
test.failing("FooBarQix_NumberObeyNoRule_DisplayIt", testEquals, 4, "4");
test.failing("FooBarQix_NumberObeyNoRule_DisplayIt", testEquals, 8, "8");

test.failing("FooBarQix_NumberIsDivisibleByThree_DisplayFoo", testContains, 3, "Foo");
test.failing("FooBarQix_NumberIsDivisibleByThree_DisplayFoo", testContains, 6, "Foo");
test.failing("FooBarQix_NumberIsDivisibleByThree_DisplayFoo", testContains, 9, "Foo");

test.failing("FooBarQix_NumberContainsThree_DisplayFoo", testContains, 3, "Foo");
test.failing("FooBarQix_NumberContainsThree_DisplayFoo", testContains, 13, "Foo");
test.failing("FooBarQix_NumberContainsThree_DisplayFoo", testContains, 23, "Foo");
test.failing("FooBarQix_NumberContainsThree_DisplayFoo", testContains, 33, "Foo");

test.failing("FooBarQix_NumberContainsThreeAndIsDivisibleByThree_DisplayFooFoo", testEquals, 3, "FooFoo");

test.failing("FooBarQix_NumberIsDivisibleByFive_DisplayBar", testContains, 5, "Bar");
test.failing("FooBarQix_NumberIsDivisibleByFive_DisplayBar", testContains, 10, "Bar");
test.failing("FooBarQix_NumberIsDivisibleByFive_DisplayBar", testContains, 15, "Bar");

test.failing("FooBarQix_NumberContainsFive_DisplayBar", testContains, 5, "Bar");
test.failing("FooBarQix_NumberContainsFive_DisplayBar", testContains, 15, "Bar");
test.failing("FooBarQix_NumberContainsFive_DisplayBar", testContains, 25, "Bar");
test.failing("FooBarQix_NumberContainsFive_DisplayBar", testContains, 35, "Bar");

test.failing("FooBarQix_NumberIsDivisibleBySeven_DisplayQix", testContains, 7, "Qix");
test.failing("FooBarQix_NumberIsDivisibleBySeven_DisplayQix", testContains, 14, "Qix");
test.failing("FooBarQix_NumberIsDivisibleBySeven_DisplayQix", testContains, 21, "Qix");

test.failing("FooBarQix_NumberContainsSeven_DisplayQix", testContains, 7, "Qix");
test.failing("FooBarQix_NumberContainsSeven_DisplayQix", testContains, 17, "Qix");
test.failing("FooBarQix_NumberContainsSeven_DisplayQix", testContains, 27, "Qix");
test.failing("FooBarQix_NumberContainsSeven_DisplayQix", testContains, 37, "Qix");

test.failing("FooBarQix_DividersBeforeContent", testEquals, 51, "FooBar");

test.failing("FooBarQix_DigitRuleIsTreatedIsTheRightOrder", testEquals, 53, "BarFoo");

test.failing("FooBarQix_DivideRuleIsTreatedIsTheRightOrder", testEquals, 21, "FooQix");

test.failing("FooBarQix_MultipleDividersAndDigits", testEquals, 15, "FooBarBar");

test.failing("FooBarQix_MultipleDividersAndDigitsAll_Foo", testEquals, 33, "FooFooFoo");

test.failing("FooBarQix_MiscTest", testEquals, 105, "FooBarQixBar");
test.failing("FooBarQix_MiscTest", testEquals, 315, "FooBarQixFooBar");
test.failing("FooBarQix_MiscTest", testEquals, 735, "FooBarQixQixFooBar");
