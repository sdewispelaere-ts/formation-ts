import { test } from "ava";
import { Loop } from "../src/loop";

test.failing("Quand on ajoute un élément avec push, on le récupère avec pop", t => {
    const loop = new Loop<string>();
    loop.push("a");
    loop.push("b");
    loop.push("c");
    t.is("c", loop.pop());
});