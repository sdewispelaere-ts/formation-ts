import { test } from "ava";
import { hello } from "../src/example";

test(t => {
    t.is("Hello world", hello());
});